FROM node:11-alpine

ENV THEIA_DEFAULT_PLUGINS=local-dir:plugins \
    WORKSPACE_DIR=/tmp

COPY package.json /app/package.json

WORKDIR /app

RUN apk --update add \
        bash \
        python \
        python3 \
        make \
        wget \
        curl \
        git && \
    apk --update add --virtual .deps \
        build-base && \
    yarn && \
    yarn theia build && \
    adduser -D -S -h /app -s /bin/sh -G root --uid 1001 theia && \
    chown -R 1001:0 /app && \
    chmod -R g=u /app && \
    apk del .deps

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

USER 1001

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD yarn start ${WORKSPACE_DIR} --hostname 0.0.0.0 --port 8080
